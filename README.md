# slock - simple screen locker

This is my fork of slock.

## applied patches

- auto timeout (aka Auto Timeoff)
- dpms
- capscolor

## maybe patches

- secret password
- quickcancel
- mediakeys
- dwmlogoandblurscreen OR foreground and background

# original description

simple screen locker utility for X.


## Requirements

In order to build slock you need the Xlib header files.


## Installation

Edit config.mk to match your local setup (slock is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install slock
(if necessary as root):

    make clean install


## Running slock

Simply invoke the 'slock' command. To get out of it, enter your password.
