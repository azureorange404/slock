/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nobody";

static const char *colorname[NUMCOLS] = {
	[INIT]   = "#111111",     /* after initialization */
	[INPUT]  = "#255957",   /* during input */
	[FAILED] = "#a63d40",   /* wrong password */
	[CAPS]   = "#DDB84A",         /* CapsLock on */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;

/* time in seconds before the monitor shuts down */
static const int monitortime = 10;

/* Patch: auto-timeout */
/* should [command] be run only once? */
static const int runonce = 1;
/* length of time (seconds) until [command] is executed */
static const int timeoffset = 60;
/* command to be run after [timeoffset] seconds has passed */
static const char *command = "/usr/bin/systemctl suspend -i";
